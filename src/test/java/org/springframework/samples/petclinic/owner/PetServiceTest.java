package org.springframework.samples.petclinic.owner;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.springframework.samples.petclinic.utility.PetTimedCache;

import static org.mockito.Mockito.*;

class PetServiceTest {
	// SUT
	PetService petService;
	// Mock
	PetTimedCache petTimedCache;
	// Stub
	OwnerRepository ownerRepository = mock(OwnerRepository.class);
	// Spy
	Logger logger = mock(Logger.class);

	Owner owner;
	Pet pet;


	@BeforeEach
	public void setup(){
		petTimedCache = mock(PetTimedCache.class);
		ownerRepository = mock(OwnerRepository.class);
		logger = mock(Logger.class);
		petService = new PetService(petTimedCache, ownerRepository, logger);

		owner = new Owner();
		owner.setId(1);
		owner.setFirstName("pomid");
		when(ownerRepository.findById(1)).thenReturn(owner);

		pet = new Pet();
		pet.setId(2);
		pet.setName("felfel");
		when(petTimedCache.get(2)).thenReturn(pet);
	}

	// Classical
	@Test
	public void findOwnerTest() {
		Owner o = ownerRepository.findById(1);
		// state verification
		Assertions.assertEquals(o, owner);
		// behavior verification
		verify(logger).info("find owner " + Integer.toString(o.getId()));
	}

	// Mockisty
	@Test
	public void newPetTest(){
		// behavior verification
		Pet pet = petService.newPet(owner);

		// state verification
		Assertions.assertTrue(pet.getOwner().equals(owner));
		// behavior verification
		verify(logger, times(1))
			.info("add pet for owner " + Integer.toString(owner.getId()));
		// should the next line run too ? it looks like it tests something else
		//Assertions.assertTrue(owner.getPets().contains(pet));
	}

	// Mockisty
	@Test
	public void findPetTest(){
		// state verification
		Assertions.assertEquals(pet, petService.findPet(pet.getId()));
		// behavior verification
		verify(logger, times(1))
			.info("find pet by id " + Integer.toString(pet.getId()));
	}

	// Mockisty
	@Test
	public void savePetTest(){
		// state verification
		Pet petx = new Pet();
		petx.setId(-1);

		verify(logger).info("save pet " + Integer.toString(petx.getId()));
		verify(petTimedCache).save(petx);
	}

}
