package org.springframework.samples.petclinic.utility;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class SimpleDIImpl extends SimpleDI {

	Map<Class<?>, Object> instances = new HashMap<Class<?>, Object>();

	static SimpleDIImpl instance;

	SimpleDIImpl() {
	}

	static {
		instance = new SimpleDIImpl();
	}

	public static SimpleDIImpl getInstance() {
		return instance;
	}

	public void provideByInstance(Class<?> typeClass, Object instanceOfType) {
		if (instances.containsKey(typeClass)){
			// should i throw an exception here ?
		}
		instances.put(typeClass, instanceOfType);
	}

	public void provideByAConstructorFunction(Class<?> typeClass, Callable<Object> providerFunction) {
		try {
			instances.put(typeClass, providerFunction.call());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Object getInstanceOf(Class<?> requiredType) throws Exception {
		if (instances.containsKey(requiredType))
			return instances.get(requiredType);
		throw new Exception("cannot provide this type " + requiredType.toString());
	}

}

/* wasn't sure if this is what u wanted so kept this one bcs it does pass the tests
public class SimpleDIImpl extends SimpleDI {

	Map<Class<?>, Object> instances = new HashMap<Class<?>, Object>();

	Map<Class<?>, Callable<Object>> constructors = new HashMap<Class<?>, Callable<Object>>();

	static SimpleDIImpl instance;

	SimpleDIImpl() {
	}

	static {
		instance = new SimpleDIImpl();
	}

	public static SimpleDIImpl getInstance() {
		return instance;
	}

	public void provideByInstance(Class<?> typeClass, Object instanceOfType) {
		instances.put(typeClass, instanceOfType);
	}

	public void provideByAConstructorFunction(Class<?> typeClass, Callable<Object> providerFunction) {
		constructors.put(typeClass, providerFunction);
	}

	public Object getInstanceOf(Class<?> requiredType) throws Exception {
		if (instances.containsKey(requiredType))
			return instances.get(requiredType);
		else if (constructors.containsKey(requiredType))
			return constructors.get(requiredType).call();
		throw new Exception("cannot provide this type " + requiredType.toString());
	}

}
*/
